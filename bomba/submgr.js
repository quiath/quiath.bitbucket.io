﻿  // UTF-8
  // Safari performance polyfill
  window.performance = (window.performance || {
    offset: Date.now(),
    now: function now(){
        return Date.now() - this.offset;
    }
  });

  // util

  function findSelectedPara() {
    var sel = window.getSelection();
    if (sel != undefined && sel.rangeCount > 0) {
      var nd = sel.getRangeAt(0).startContainer;
      if (nd != undefined) {
        while (nd.nodeName != "P" && nd.parentNode != undefined)
          {
            nd = nd.parentNode;
          }
          if (nd != undefined && nd.nodeName === "P" ) {

            return nd; 
          }
      }
    }
    return null;
  }

  function nonNeg(a) {
    if (isNaN(a)) {
      return a;
    }
    a = Math.round(a);
    return a > 0 ? a : 0;
  }

  // modify begin/end with Insert

  function createInsertOffsetBeginSE(frame, optAddFrames) {
    if (isNaN(optAddFrames)) {
      return function (oldBegin, oldEnd) { 
        return nonNeg(frame).toString(); 
      }
    } else {
      return function (oldBegin, oldEnd) { 
        if (isNaN(oldBegin)) { 
          return nonNeg(frame).toString(); 
        }
        return nonNeg(oldBegin + optAddFrames).toString();
      }
    }
  }

  function createInsertOffsetEndSE(frame, optAddFrames) {
    if (isNaN(optAddFrames)) {
      return function (oldBegin, oldEnd) { 
        if (isNaN(oldBegin) || isNaN(oldEnd)) {
          return isNaN(oldEnd) ? "" : nonNeg(oldEnd).toString(); 
        }
        var oldDiff = oldEnd - oldBegin;
        return nonNeg(frame + oldDiff).toString(); 
      }
    } else {
      return function (oldBegin, oldEnd) { 
        if (isNaN(oldBegin) || isNaN(oldEnd)) {
          return isNaN(oldEnd) ? "" : nonNeg(oldEnd).toString(); 
        }

        return isNaN(oldEnd) ? "" : nonNeg(oldEnd + optAddFrames).toString(); 
      }
    }
  }

  // modify begin/end with Alt-Insert



  //////////////////////////////////////
  // SubtitleMgr
  // //////////////////////////////////

  function extractFramesFromText(txt) {
    if (txt === null || txt === undefined) {
      return {brackets: 0};
    }

    var reg2=/^\s*{(-?[0-9]+)}{(-?[0-9]+)}(.*)/;
    var reg1=/^\s*{(-?[0-9]+)}(.*)/;

    var m = txt.match(reg2);
    if (m) {
      var t0 = parseInt(m[1]);
      var t1 = parseInt(m[2]);
      return {brackets : 2, frameBegin: t0, frameEnd: t1};
    }
    m = txt.match(reg1);
    if (m) {
      var t0 = parseInt(m[1]);
      return {brackets: 1, frameBegin: t0, frameEnd: null};
    }  

    return {brackets: 0};
  }

  function makeTimecode(frameBegin, frameEnd)
  {
    var newTimeCode =  isNaN(frameBegin) ? "{}" : "{" + frameBegin + "}";
    newTimeCode += isNaN(frameEnd) ? "{}" : "{" + frameEnd + "}"; 
    return newTimeCode;
  }

  function replaceFramesInText(txt, nBrackets, frameBegin, frameEnd) {
    var repRegFix = /{-?[0-9]+}({(-?[0-9]+)?})?/; 
    var repReg2 = /{-?[0-9]+}{-?[0-9]+}/; 
    var repReg1 = /{-?[0-9]+}{}/;

    if (nBrackets == this.REPLACE_ALL_WITH_TWO) {
      txt = txt.replace(repRegFix, "{" + frameBegin + "}{" + frameEnd + "}");
      return txt;    
    }
    if (nBrackets == 2) {
      txt = txt.replace(repReg2, "{" + frameBegin + "}{" + frameEnd + "}");
      return txt;
    }
    if (nBrackets == 1) {
      txt = txt.replace(repReg1, "{" + frameBegin + "}");
      return txt;
    }  
    return txt;
  }

  function shiftByFramesImpl(elem, childTag, frameDelta, rangeStart, rangeStop) {
    rangeStart = rangeStart || -Infinity;
    rangeStop = rangeStop || Infinity;

    frameDelta = parseInt(frameDelta) || 0;

    var children = elem.getElementsByTagName(childTag);
    var parsedLines = this.parseLines(children, this.REGEX_OPT_OBJECT, this.REGEX_COMMENT);

    for (var i = 0; i < children.length; ++i) {
      var line = parsedLines[i];
      if (line.type == this.TYPE_SUBTITLE && !isNaN(line.begin) && line.begin >= rangeStart && line.begin <= rangeStop) {  
        var newTimeCode = this.replaceFramesInText(line.timeText, 
                                                   this.REPLACE_ALL_WITH_TWO, 
                                                   line.begin + frameDelta, 
                                                   isNaN(line.end) ? "" : (line.end + frameDelta).toString()); 
        // this is actually a shorter version of the above but does not reuse that function
        // var newTimeCode =  "{" + (line.begin + frameDelta) + "}";
        // newTimeCode += isNaN(line.end) ? "{}" : "{" + (line.end + frameDelta) + "}"; 

        insertIntoNode(elem, children[i], newTimeCode + line.text);
      }
    }
  }



  function parseSingleLine(txt, regexGoodObj, regexComment, last) {
    if (txt.match(regexComment)) {
      return {className: "comment", text: txt, type: this.TYPE_COMMENT};
    }
    var regexGood = regexGoodObj.regex;
    var m = txt.match(regexGood);
    if (m) {
      var obj = {className: "normal", 
        text: m[regexGoodObj.tIndex], 
      begin: parseInt(m[regexGoodObj.bIndex]), 
      end: parseInt(m[regexGoodObj.eIndex]),
      prev: last,
      type: this.TYPE_SUBTITLE,
      timeText: m[regexGoodObj.qIndex]
      };
      return obj;
    }
    return {className: "error", text: txt, type: this.TYPE_UNKNOWN};
  }

  function parseLines(children, regexGoodObj, regexComment) {
    var out = [];
    var last = undefined;
    for(var i = 0; i < children.length; ++i) {
      var s = children[i].textContent.replace(/\n\s*/g, " ");
      var obj = this.parseSingleLine(s, regexGoodObj, regexComment, last);
      if (obj.type === this.TYPE_SUBTITLE) {
        if (last !== undefined) {
          out[last].next = i;
        }
        last = i;
      }
      out.push(obj);
    }
    return out;
  }

  function getColorClass(curr, prevBeginFrame, limitFrame, charsPerFrame) {
    if (curr.type == this.TYPE_COMMENT) {
      return "comment";
    } else if (curr.type == this.TYPE_SUBTITLE) {
      if (!isNaN(prevBeginFrame) && prevBeginFrame !== undefined && prevBeginFrame >= curr.begin) { 
        return "error";
      }

      if (isNaN(curr.end) || curr.end === undefined || curr.end <= curr.begin) {
        return "error";
      } else {
        if (!isNaN(limitFrame) && limitFrame !== undefined && curr.end > limitFrame) {
          return "exceeded";
        }
        var len = curr.text.length;
        var df = curr.end + 1 - curr.begin;
        var ratio = len / (charsPerFrame * df);
        if (ratio > 2) {
          return "shortest";
        } 
        if (ratio > 1) {
          return "short";
        }       
      }
      return "normal";
    }
    return "error"; 
  }

  function processSubsImpl(elem, childTag, frameRate, minT, maxT, intervalT, charsPerSec, logElem, mode) {

    var t0 = performance.now();

    var logEnabled = (mode == this.MODE_REPAIR);

    logElem = logElem || { innerHTML : "" };  

    var log = function(x) { 
      if (logEnabled) { 
        logElem.innerHTML = ""; 
        log = function(x) { if (logEnabled) { logElem.innerHTML += "<p>" + x + "<\/p>"; } }
        log(x);
      } 
    }

    childTag = childTag || "p";
    frameRate = parseFloat(frameRate) || -1;
    if (frameRate <= 0) {
      logElem.innerHTML += "<p>Invalid frame rate<\/p>"
    }


    minT = parseFloat(minT) || 1.0;
    maxT = parseFloat(maxT) || 5.0;
    intervalT = parseFloat(intervalT) || 0.08;

    var f2t = function (x) { return x / frameRate; }; 
    var t2f = function (x) { return Math.round(x * frameRate); }; 


    var charsPerFrame = charsPerSec * f2t(1);

    var children = elem.getElementsByTagName(childTag);
    var parsedLines = this.parseLines(children, this.REGEX_OPT_OBJECT, this.REGEX_COMMENT);
    for (var i = 0; i < parsedLines.length; ++i) {
      //console.log(parsedLines[i]);
      var curr = parsedLines[i];
      var savEnd = curr.end;
      var lineNo = i + 1;
      var prevLine = curr.prev !== undefined ? parsedLines[curr.prev] : undefined;
      var nextLine = curr.next !== undefined ? parsedLines[curr.next] : undefined;
      var maxFrame = (nextLine !== undefined && nextLine.begin !== undefined) ? nextLine.begin - t2f(intervalT) : Infinity;
      var prevBeginFrame = (prevLine !== undefined ? prevLine.begin : -Infinity) || -Infinity;

      if (mode === this.MODE_COLORIZE) { // colorize and generate only
        var cn = this.getColorClass(curr, prevBeginFrame, maxFrame, charsPerFrame);
        children[i].className = cn;
        parsedLines[i].display = curr.type == this.TYPE_SUBTITLE && cn != "error";
        continue;
      }

      if (curr.type == this.TYPE_SUBTITLE) {
        if (prevBeginFrame >= curr.begin) {
          log("Błąd: linijka " + lineNo + " zaczyna się przed poprzednią");
          children[i].className = "error";
          continue;
        }

        if (isNaN(curr.end) || curr.end === undefined) {
          log("Ostrzeżenie: brak czasu końca napisu w linii " + lineNo + ", dodano czas końca");
          var e = curr.text.length / Math.max(1, charsPerSec);
          curr.end = curr.begin + t2f(e);  
        }
        // assert curr.end !== undefined
        var frameLen = curr.end - curr.begin
        var e = f2t(frameLen);
        var ne = Math.min(maxT, Math.max(minT, e));
        var newFrameLen = t2f(ne); 
        if (newFrameLen != frameLen) {
          log("Ostrzeżenie: poprawiono długość trwania napisu do " + ne + "s w linijce " + lineNo);
          curr.end = curr.begin + newFrameLen;
        }

        if (curr.next !== undefined) {
          //var nextLine = parsedLines[curr.next];
          //var maxFrame = nextLine.begin - t2f(intervalT);
          if (maxFrame <= curr.begin) {
            log("Błąd nienaprawialny automatycznie: początki napisów zbyt blisko siebie w linijce " + lineNo);
            children[i].className = "error";
            children[curr.next].className = "error";
          } else {
            if (maxFrame < curr.end) {
              log("Ostrzeżenie: skrócono czas napisu w linijce " + lineNo + " z uwagi na inny napis");
              curr.end = maxFrame;
            }
          }
        }      
        if (savEnd !== curr.end) {
          //console.log("replace " + children[i].innerHTML);
          //children[i].innerHTML = this.replaceFramesInText(children[i].innerHTML, this.REPLACE_ALL_WITH_TWO, curr.begin, curr.end);
          var timeCode = makeTimecode(curr.begin, curr.end); // takes care of NaN 
          if (timeCode != curr.timeText) {
            insertIntoNode(elem, children[i], timeCode + curr.text);
          }
          //console.log("replaced " + children[i].innerHTML);        
        }
      } // if subtitle
    } // for

    var out = [];
    if (mode === 0) {
      out = parsedLines.filter(function(e){ return e.display; } );
    }

    var t1 = performance.now();
    //logEnabled = true;
    log("Czas przetwarzania: " + (t1 - t0) + "ms");

    return out;

  }

  function removeEmpty(elem, childTag) {
    childTag = childTag || "p";
    //var children = elem.getElementsByTagName(childTag);
    var children = [].slice.call(elem.getElementsByTagName(childTag));

    for (var i = 0; i < children.length; ++i) {
      var t = children[i].textContent.trim();
      if (t === "") {
        children[i].hidden = true;
        elem.removeChild(children[i]);
      }
    }
  }

  function trimText(elem, childTag) {
    childTag = childTag || "p";
    var children = elem.getElementsByTagName(childTag);
    var parsedLines = this.parseLines(children, this.REGEX_OPT_OBJECT, this.REGEX_COMMENT);

    for (var i = 0; i < children.length; ++i) {
      var line = parsedLines[i];
      if (line.type == this.TYPE_SUBTITLE) {

        var a = line.text.split("|");
        var atrim = a.map(function (x) { return x.trim(); });
        var t = atrim.join("|");

        if (t != line.text) {
          console.log("Trimmed: " + line.text);
          //children[i].innerHTML = line.timeText + t;
          insertIntoNode(elem, children[i], line.timeText + t);
        }
      }
    }
  }

  function insertIntoNode(elem, nd, addText, beginIndex, endIndex)
  {
    if (nd == undefined) {
      console.log("Assertion failed, undefined instead of node");
      return false;
    }
    if (nd.nodeName !== "P") {
      console.log("Assertion failed, P expected, got: " + nd.nodeName);
      return false;
    }
    var textNode = nd.firstChild;
    if (!textNode || textNode.nodeType !== 3) {
      console.log("Assertion failed, text node expected, got: " + (textNode ? textNode.nodeType : "no first child"));
      return false;
    } 
    if (beginIndex == undefined) {
      beginIndex = 0;
    }
    if (endIndex == undefined) {
      endIndex = nd.textContent.length;
    }
    var sel = window.getSelection();
    var range = document.createRange();
    range.setStart(textNode, beginIndex);
    range.setEnd(textNode, endIndex);
    sel.removeAllRanges();
    sel.addRange(range);
    document.execCommand('insertText', false, addText);
    sel.removeAllRanges();
    return true;
  }

  function compressSpaces(elem, childTag) {
    //var regSpaces = / +/g;

    // store the previous caret position (only the para)
    elem.focus();
    var prevSelNode = findSelectedPara();
    if (prevSelNode && prevSelNode.nodeType === 1) {
      console.log("Was: " + prevSelNode.textContent);
    }

    var regSpaces = /\s{2,}/g;
    childTag = childTag || "p";
    var children = elem.getElementsByTagName(childTag);
    var parsedLines = this.parseLines(children, this.REGEX_OPT_OBJECT, this.REGEX_COMMENT);

    for (var i = 0; i < children.length; ++i) {
      var line = parsedLines[i];
      if (line.type == this.TYPE_SUBTITLE) {
        var t = line.text.replace(regSpaces, " ");
        if (t != line.text) {
          console.log("Compressed: " + line.text);
          //children[i].innerHTML = line.timeText + t;
          insertIntoNode(elem, children[i], line.timeText + t);
        }
      }
    }

    // restore the caret position (to the start of the para)
    if (prevSelNode) {
      var sel = window.getSelection();
      sel.collapse(prevSelNode, 0);
    }
  }

  function divideLinesImpl(elem, childTag, replaceOld, charsPerLine) {
    elem.focus();
    var regWhitespace = /\s{1,}/g;
    childTag = childTag || "p";
    charsPerLine = parseInt(charsPerLine) || 40;
    var children = elem.getElementsByTagName(childTag);
    var parsedLines = this.parseLines(children, this.REGEX_OPT_OBJECT, this.REGEX_COMMENT);
    for (var i = 0; i < children.length; ++i) {
      var line = parsedLines[i];
      if (line.type == this.TYPE_SUBTITLE) {
        var t = line.text;

        if (!replaceOld && t.indexOf('|') >= 0) {
          continue;
        }
        
        t = t.replace(/\|/g, " ");
        var out = [];

        if (t.length <= charsPerLine) {
          continue;
        }

        while (t.length > charsPerLine) {

          var tmp = t.substr(0, charsPerLine);
          var m = tmp.match(/\s(?!.*\s)/); // (?! negative lookahead
          if (m == null) {
            // a superlong word, force split
            out.push(tmp);
            t = t.substr(charsPerLine);
          } else {
            // skip whitespace
            out.push(tmp.substr(0, m.index).trim());  // trim to avoid accumulating spaces
            t = t.substr(m.index + 1);
          }
        }

        if (t.trim().length > 0) {
          out.push(t.trim());
        }

        var joined = out.join("|");
        //children[i].innerHTML = line.timeText + joined;
        insertIntoNode(elem, children[i], line.timeText + joined)
      }
    }
  }

  function insertTimecode(elem, frame, moveDown) {
    // insert or update?
    var updated = false;
    var nd = findSelectedPara();
    if (nd != undefined) {
      var s = nd.textContent;
      console.log("'"+s+"'");
      var r = /^\s*{([0-9]+)}/;
      var m = s.match(r);
      if (m != null) {
        //replace via HTML
        var s = nd.innerHTML;
        var r = /{([0-9]+)}/;
        if (s.match(r)) {
          var framestr = "{" + frame + "}";
          var ns = s.replace(r, framestr);

          updated = true;
          nd.innerHTML = ns;
        } else {
          console.log("Nothing to replace");
        }

      }
    }
    if (!updated) {
      var sel = window.getSelection();
      if (nd != undefined) {
        sel.collapse(nd.firstChild, 0);
      }
      document.execCommand('insertText', false, "{" + frame + "}");
    }

    if (moveDown) {
      nd = findSelectedPara();
      sel = window.getSelection();
      if (nd != undefined) {
        var offset0 = nd.offsetTop;
        for (;;) {
          console.log(nd.nodeName + " " + nd.nodeType + nd.textContent);
          nd = nd.nextSibling;
          if (nd == undefined) { // null too
            break;
          } 
          if (nd.nodeType === 1 && nd.nodeName === "P") {
            sel.collapse(nd.firstChild, 0);
            var offset1 = nd.offsetTop;
            console.log("Offset ", offset1 - offset0);

            elem.scrollTop += offset1 - offset0;
            break;
          }
        }
      }
    }
  }

  function moveSelectionDownImpl(elem) {
    var nd = findSelectedPara();
    var sel = window.getSelection();
    if (nd != undefined) {

      var offset0 = nd.offsetTop;
      for (;;) {
        console.log(nd.nodeName + " " + nd.nodeType + nd.textContent);
        nd = nd.nextSibling;
        if (nd == undefined) { // null too
          break;
        } 
        if (nd.nodeType === 1 && nd.nodeName === "P" && !nd.textContent.match(/^\s*$/)) {
          sel.collapse(nd.firstChild, 0);
          var offset1 = nd.offsetTop;
          console.log("Offset ", offset1 - offset0);

          var sy = nd.getBoundingClientRect().top;
          var er = elem.getBoundingClientRect();
          if (sy < er.top + er.height / 2) {
            return;
          }

          elem.scrollTop += offset1 - offset0;
          break;
        }
      }
    }
  }

  function insertTimecodeEndToNode(obj, node, frame, optAddFrames) {  
    if (obj.type === this.TYPE_SUBTITLE) { 
      var begin = isNaN(obj.begin) ? "" : obj.begin;
      if (!isNaN(optAddFrames)) {
        // add optional frames to {frame} if exists or time from video otherwise
        frame = isNaN(obj.end) ? (frame + optAddFrames) : (obj.end + optAddFrames);
      }      
      frame = Math.max(0, frame);
      node.innerHTML = "{" + begin + "}{" + frame + "}" + obj.text;
    } else if (obj.type === this.TYPE_UNKNOWN) {
      node.innerHTML = "{}{" + frame + "}" + obj.text;
    }
  }  

  function insertTimecodeBeginToNode(obj, node, frame, optAddFrames) {
    if (obj.type === this.TYPE_SUBTITLE) { 
      var end = isNaN(obj.end) ? "" : obj.end;
      if (!isNaN(optAddFrames)) {
        // add optional frames to {frame} if exists or time from video otherwise
        frame = isNaN(obj.begin) ? (frame + optAddFrames) : (obj.begin + optAddFrames);
      }
      frame = Math.max(0, frame);      
      node.innerHTML = "{" + frame + "}{" + end + "}" + obj.text;
    } else if (obj.type === this.TYPE_UNKNOWN) {
      node.innerHTML = "{" + frame + "}{}" + obj.text;
    }
  }

  function insertTimecodeImpl(elem, frame, moveDown, inserter, optAddFrames) {
    // insert or update?
    var updated = false;
    var nd = findSelectedPara();
    if (nd != undefined) {
      // nd nodeName === "P"
      var obj = this.parseSingleLine(nd.textContent.replace(/\n/g, " "), this.REGEX_BEGOPT_ENDOPT_OBJECT, this.REGEX_COMMENT, 0);
      inserter.call(this, obj, nd, frame, optAddFrames); // pass this

      if (moveDown) {
        this.moveSelectionDown(elem);
      }
    }
  }

  function allChildrenArePara(node) {
    var childNodes = node.childNodes;
    for(var i = 0; i < childNodes.length; ++i) {
      var ch = childNodes[i];
      if (ch.nodeType !== 1 || ch.nodeName !== "P") {
        return false;
      }
    }
    return true;
  }

  function flattenToPara(node) {
    // return string for innerHTML
    s = "";
    openP = "";

    var addP = function() {
      if (openP !== "") {
        s += "<p>" + openP + "<\/p>";
        openP = "";
      }
    };
    var childNodes = node.childNodes;
    for(var i=0; i<childNodes.length; i++) {
        var ch = childNodes[i];
        if (ch.nodeType === 3) {
            openP += ch.nodeValue;
        } else if (ch.nodeType === 1) {
            if (ch.nodeName === "P") {
                // good
                addP();
                s += (ch.outerHTML || "");
            } else if (ch.nodeName === "BR") {
                addP();               
            } else if (ch.nodeName === "DIV") {
                addP();                               
                s += flattenToPara(ch);
            } else if (ch.nodeName === "PRE") {
                addP();  
                var ss = "<p>" + 
                         ch.innerHTML.replace(/\n/g, "<\/p><p>") + 
                         "<\/p>";
                s += ss;
            } else {
                openP += (ch.textContent || "");
            }
        }
        //if 
    }
    addP();   
    return s;
  }

  function changeSubtitleFPSImpl(elem, childTag, fpsOld, fpsNew) {
    //var regWhitespace = /\s{1,}/g;
    childTag = childTag || "p";
    fpsOld = parseFloat(fpsOld) || 25.0;
    fpsNew = parseFloat(fpsNew) || 25.0;
    var children = elem.getElementsByTagName(childTag);
    var parsedLines = this.parseLines(children, this.REGEX_OPT_OBJECT, this.REGEX_COMMENT);
    for (var i = 0; i < children.length; ++i) {
      var line = parsedLines[i];
      if (line.type == this.TYPE_SUBTITLE) {
        var fBegin = Math.round(line.begin / fpsOld * fpsNew);
        var fEnd = Math.round(line.end / fpsOld * fpsNew);
        var timeCode = makeTimecode(fBegin, fEnd); // takes care of NaN
        insertIntoNode(elem, children[i], timeCode + line.text); // TODO: replace whole text at once to avoid undo problems 
      }

    }
  }

  function stretchSubtitlesImpl(elem, childTag, frameFirst, timeFirst, frameLast, timeLast, frameRate) {
    //var regWhitespace = /\s{1,}/g;
    childTag = childTag || "p";
    timeFirst = parseFloat(timeFirst) || 0.0;
    timeLast = parseFloat(timeLast) || 0.0;
    frameFirst = parseInt(frameFirst) || 0;
    frameLast = parseInt(frameLast) || 0;    
    frameRate = parseFloat(frameRate) || 25.0;    
    console.log(timeFirst  + " - " + timeLast);
    if (timeFirst >= timeLast) {
      return -1;
    }
    if (frameFirst >= frameLast) {
      return -2;
    }

    var newFrameFirst = Math.round(timeFirst * frameRate);
    var newFrameLast = Math.round(timeLast * frameRate);

    var replacedLines = 0;

    var children = elem.getElementsByTagName(childTag);
    var parsedLines = this.parseLines(children, this.REGEX_OPT_OBJECT, this.REGEX_COMMENT);
    for (var i = 0; i < children.length; ++i) {
      var line = parsedLines[i];
      if (line.type == this.TYPE_SUBTITLE) {
        var oldBegin = line.begin;
        if (oldBegin >= frameFirst && oldBegin <= frameLast) {
          var oldEnd = line.end;
          var newBegin = Math.round((oldBegin - frameFirst) * (newFrameLast - newFrameFirst) / (frameLast - frameFirst) + newFrameFirst);
          var newEnd = Math.round((oldEnd - frameFirst) * (newFrameLast - newFrameFirst) / (frameLast - frameFirst) + newFrameFirst);
          var timeCode = makeTimecode(newBegin, newEnd); // takes care of NaN
          insertIntoNode(elem, children[i], timeCode + line.text); // TODO: replace everything at once
          // children[i].innerHTML = "{" + newBegin + "}{" + newEnd + "}" + line.text; 
          ++replacedLines;
        }
      }
    }
    return replacedLines;
  }

  function SubtitleMgr(editDiv, frameRate, duration, installFrameCatch)
  {
    this.editDiv = editDiv;
    this.frameRate = frameRate;
    this.duration = duration;

    this.childTag = "p";

    this.REGEX_BOTH = /^\s*{([0-9]+)}{([0-9]+)}(.*)/;
    this.REGEX_OPT = /^\s*({([0-9]+)}({([0-9]*)})?)(.*)/;
    this.REGEX_OPT_OBJECT = { regex: this.REGEX_OPT, bIndex: 2, eIndex: 4, tIndex: 5, qIndex: 1 };
    this.REGEX_BEGOPT_ENDOPT = /^\s*({([0-9]*)}({([0-9]*)})?)(.*)/;
    this.REGEX_BEGOPT_ENDOPT_OBJECT = { regex: this.REGEX_BEGOPT_ENDOPT, bIndex: 2, eIndex: 4, tIndex: 5, qIndex: 1 };
    this.REGEX_COMMENT = /^\s*;.*/;

    this.TYPE_UNKNOWN = 1;
    this.TYPE_COMMENT = 2;
    this.TYPE_SUBTITLE = 3;

    this.MODE_COLORIZE = 0;
    this.MODE_REPAIR = 1;

    this.REPLACE_ALL_WITH_TWO = 10;

    this.preserveCaret = true;
    this.subEditCompat = true;
    this.compactInnerText = true;

    this.updateFrameRate = function (frameRate) { this.frameRate = frameRate; this.logStatus();}
    this.updateDuration = function (duration) { this.duration = duration; this.logStatus(); }

    this.extractFramesFromText = extractFramesFromText;      //
    this.replaceFramesInText = replaceFramesInText;          //

    this.shiftByFrames = function(frameDelta, rangeStart, rangeStop) 
    {
      return shiftByFramesImpl.call(this, this.editDiv, this.childTag, frameDelta, rangeStart, rangeStop);
    }

    this.parseSingleLine = parseSingleLine;                  // 
    this.parseLines = parseLines;                            //          
    this.getColorClass = getColorClass;                      //

    this.processSubs = function(frameRate, minT, maxT, intervalT, charsPerSec, logElem, mode) { 
      return processSubsImpl.call(this, this.editDiv, this.childTag, frameRate, minT, maxT, intervalT, charsPerSec, logElem, mode); 
    };

    this.removeEmpty = function() { return removeEmpty.call(this, this.editDiv, this.childTag); }; 
    this.trimText = function() { return trimText.call(this, this.editDiv, this.childTag); };
    this.compressSpaces = function() { return compressSpaces.call(this, this.editDiv, this.childTag); };
    this.divideLines = function(replaceOld, charsPerLine) { return divideLinesImpl.call(this, this.editDiv, this.childTag, replaceOld, charsPerLine); };
    this.insertTimecode = function(frame, moveDown) { return insertTimecode.call(this, this.editDiv, frame, moveDown); };
    
    this.moveSelectionDown = function() { return moveSelectionDownImpl.call(this, this.editDiv); };     

    this.insertTimecodeEnd = function(frame, moveDown, optAddFrames) { 
      if (!this.preserveCaret) {
        return insertTimecodeImpl.call(this, this.editDiv, frame, moveDown, insertTimecodeEndToNode, optAddFrames); 
      }
      if (isNaN(optAddFrames)) {
        return this.updateTimecodePreservingCaret(
          this.editDiv, 
          function (oldFrame, dummy) { if (isNaN(oldFrame)) return ""; return "" + nonNeg(oldFrame); },          
          function (dummy, oldFrame) { return "" + nonNeg(frame); },
          moveDown);
      } else {
        return this.updateTimecodePreservingCaret(
          this.editDiv, 
          function (oldFrame, dummy) { if (isNaN(oldFrame)) return ""; return "" + nonNeg(oldFrame); },
          function (dummy, oldFrame) { if (isNaN(oldFrame)) return "" + frame; return "" + nonNeg(oldFrame + optAddFrames); },
          moveDown);
      }      
    };

    this.insertTimecodeBegin = function(frame, moveDown, optAddFrames) { 
      if (!this.preserveCaret) {
        return insertTimecodeImpl.call(this, this.editDiv, frame, moveDown, insertTimecodeBeginToNode, optAddFrames); 
      }
      if (this.subEditCompat) {
        return this.updateTimecodePreservingCaret(
          this.editDiv, 
          createInsertOffsetBeginSE(frame, optAddFrames),
          createInsertOffsetEndSE(frame, optAddFrames),
          moveDown);
      }
      if (isNaN(optAddFrames)) {
        return this.updateTimecodePreservingCaret(
          this.editDiv, 
          function (oldFrame, dummy) { return "" + nonNeg(frame); },
          function (dummy, oldFrame) { if (isNaN(oldFrame)) return ""; return "" + nonNeg(oldFrame); },
          moveDown);
      } else {
        return this.updateTimecodePreservingCaret(
          this.editDiv, 
          function (oldFrame, dummy) { if (isNaN(oldFrame)) return "" + frame; return "" + nonNeg(oldFrame + optAddFrames); },
          function (dummy, oldFrame) { if (isNaN(oldFrame)) return ""; return "" + nonNeg(oldFrame); },
          moveDown);
      }
    };

    this.allChildrenArePara = function () { return allChildrenArePara.call(this, this.editDiv); };
    this.flattenToPara = function(par) { 
      par = par || this.editDiv; 
      //console.log(new Date()); 
      return flattenToPara.call(this, par); 
    };

    this.changeSubtitleFPS = function(fpsOld, fpsNew) { 
      return changeSubtitleFPSImpl.call(this, this.editDiv, this.childTag, fpsOld, fpsNew); 
    };

    this.stretchSubtitles = function(frameFirst, timeFirst, frameLast, timeLast, frameRate) { 
      return stretchSubtitlesImpl.call(this, this.editDiv, this.childTag, frameFirst, timeFirst, frameLast, timeLast, frameRate); 
    };
    
    this.logStatus = function() {
      console.log(this.editDiv + " " + this.frameRate + " " + this.duration);
    };

    this.flattenEditSubs = function() {
      if (!this.allChildrenArePara()) {
        if (this.editDiv.childNodes.length <= 2) {
          // the case of inserting into empty div in FF 
          // reformat without changing the text manually
          this.formatBlock();
          console.log("Flattened by formatBlock");
          return;
        }
        var s = this.flattenToPara();
        this.editDiv.innerHTML = s;
        console.log("Flattened");
      }      
    };

    this.replaceBreaks = function() {
      var r = /<br><\/?br>/g;
      this.editDiv.innerHTML = this.editDiv.innerHTML.replace(r, "<\/p><p>");      
    }

    this.formatBlock = function() {
       document.execCommand("formatBlock", false, "p");
    }    

    this.getInnerHtml = function() {
      return this.editDiv.innerHTML;
    }

    this.setInnerHtml = function(ih) {
      this.editDiv.innerHTML = ih;
    }       
    
    this.getInnerText = function() {
      if (this.compactInnerText) {
        // avoid empty lines between paragraphs
        var children = [].slice.call(this.editDiv.getElementsByTagName(this.childTag));

        var s = "";
        for (var i = 0; i < children.length; ++i) {
          var t = children[i].textContent.trim();
          //var t = children[i].textContent;
          s += t + "\n";
        }
        
        return s;
      }
      return this.editDiv.innerText;
    }

    this.setInnerText = function(txt) {
      this.editDiv.innerText = txt;
    }     

    this.updateTimecodePreservingCaret = function(elem, beginCallBack, endCallBack, moveDown) {
      var nd = findSelectedPara();
      if (nd != undefined) {      
        if (nd.nodeName !== "P") {
          console.log("Assertion failed, P expected, got: " + nd.nodeName);
          return;
        }

        var textNode = nd.firstChild;
        if (!textNode || textNode.nodeType !== 3) {
          console.log("Assertion failed, text node expected, got: " + (textNode ? textNode.nodeType : "no first child"));
          return;
        }

        // get old caret pos
        var sel = window.getSelection();
        var oldRange = sel.getRangeAt(0);
        var oldCaret = oldRange.startOffset;

        // get what needs to be replaced
        var obj = this.parseSingleLine(
          nd.textContent.replace(/\n/g, " "), 
          this.REGEX_BEGOPT_ENDOPT_OBJECT, 
          this.REGEX_COMMENT, 
          0);        

        var caret = 0;
        var endCaret = 0;
        if (obj.className == "normal") {
          endCaret = obj.timeText.length;
        }

        // new selection to be replaced
        var range = document.createRange();
        range.setStart(textNode, caret);
        range.setEnd(textNode, endCaret);
        sel.removeAllRanges();
        sel.addRange(range);

        var addText = "{" + beginCallBack(obj.begin, obj.end) + "}{" + endCallBack(obj.begin, obj.end) + "}";
        document.execCommand('insertText', false, addText);
        
        range.setStart(textNode, oldCaret + addText.length - endCaret);
        range.setEnd(textNode,  oldCaret + addText.length - endCaret);
        sel.removeAllRanges();
        sel.addRange(range);

        if (moveDown) {
          this.moveSelectionDown(elem);
        }

      }
    }

    this.focusAndScroll = function(elem, child, elemScrollTop) {
      var textNode = child.firstChild;
      if (textNode.nodeType != 3) {
        return false;
      }
      elem.focus();

      var sel = window.getSelection();
      var range = document.createRange();
      range.setStart(textNode, 0);
      range.setEnd(textNode, 0);
      sel.removeAllRanges();
      sel.addRange(range);

      elem.scrollTop = elemScrollTop;
      return true;
    }

    this.gotoLineWithFrame = function(elem, frame, childTag) {
      childTag = childTag || "P";

      var children = elem.getElementsByTagName(childTag);
      var er = elem.getBoundingClientRect();
      var zero = children.length ? children[0].offsetTop : 0;
      var lastParsed = 0;

      var parsedLines = this.parseLines(children, this.REGEX_OPT_OBJECT, this.REGEX_COMMENT);
      for (var i = 0; i < children.length; ++i) {
        var line = parsedLines[i];
        if (line.type == this.TYPE_SUBTITLE) {
          lastParsed = i;
          var b = isNaN(line.begin) ? 0 : line.begin;
          var e = isNaN(line.end) ? 0 : line.end;

          if (b <= frame && frame <= e || frame < b || i == children.length - 1) {
            var child = children[i];
            if (this.focusAndScroll(elem, child, child.offsetTop - zero - er.height / 2)) {
              return;
            }
          }
        }
      }
      if (lastParsed < children.length) {
        var child = children[lastParsed];
        this.focusAndScroll(elem, child, child.offsetTop - zero - er.height / 2);
      }

    }

    this.extractSelectedBeginFrameFromText = function () {
      var empty = "";
      var p = findSelectedPara(); 
      if (!p) {
        return empty;
      }
      var bracketInfo = this.extractFramesFromText(p.textContent);
      return (bracketInfo.brackets >= 1) ? bracketInfo.frameBegin : empty;      
    }

    this.frameValue = 0;
    if (installFrameCatch) {
      // if we want to know the frame in the current line with the cursor
      // when the user is operating another UI element
      // we don't have access to the cursor position / currect selection
      // since the selection has already moved to the other UI element
      // therefore it is necessary to save the frame extracted from the text
      // on the blur event. 
      var that = this;
      this.editDiv.addEventListener('blur', function (evt) {
        that.frameValue = that.extractSelectedBeginFrameFromText();
      });
    }

    // install paste handler to avoid HTML formatted text pasting
    // (StackOverflow)
    // tested on FF, Chrome, Safari
    if (true) {
      this.editDiv.addEventListener('paste', function (evt) {
        evt.preventDefault();
        var text = evt.clipboardData.getData("text/plain");
        document.execCommand("insertText", false, text);
      });
    }

    // this was suggested to work alongside the above solution
    // however there is no use case for it so far
    if (false) {
      this.editDiv.addEventListener('paste', function (evt) {
        if (evt.originalEvent && evt.originalEvent.clipboardData && evt.originalEvent.clipboardData.getData) {

          evt.preventDefault();
          var text = evt.originalEvent.clipboardData.getData("text/plain");
          document.execCommand("insertText", false, text);
        }
      });
    }    

    this.getFrameValue = function() {
      return this.frameValue;
    };

    this.configureDocumentEditing = function ()
    {
      document.execCommand('defaultParagraphSeparator', false, 'p');
      try {
        document.execCommand('insertBrOnReturn', false, false);
      }
      catch (error) {
        // IE throws an error if it does not recognize the command...
        console.log("Unsupported: " + error);
      }   
    };

    this.configureDocumentEditing();
   
    this.logStatus();
  }
