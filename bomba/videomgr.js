﻿  // UTF-8

  ////////////////////////////////////
  // VideoMgr
  ////////////////////////////////////


  function binFindSub(a, v) {
    v = v | 0;
    var left = 0;
    var right = a.length - 1;
    var mid = 0;
    while (left <= right) {
      mid = ((left + right) / 2) | 0;
      if (a[mid].begin <= v && a[mid].end >= v) {
        return mid;
      }
      if (a[mid].begin <= v) {
        left = mid + 1;
      } else if (a[mid].begin >= v) {
        right = mid - 1;
      }

    }
    if (a.length > 0 && a[mid].begin <= v && a[mid].end >= v) {
      return mid;
    }
    return -1;
  }



  function createVideoMgr(videoDOM) {
    var ob = {
      videoDOM: videoDOM,
      isPaused: function() { return this.videoDOM.paused; },
      play: function() { return this.videoDOM.play(); },
      pause: function() { return this.videoDOM.pause(); },
      playPause : function () { 
        if (this.videoDOM.paused) 
          this.videoDOM.play(); 
        else 
          this.videoDOM.pause();
      },
      load: function() { return this.videoDOM.load(); },
      setCurrentTime: function(t) { this.videoDOM.currentTime = t; },
      getCurrentTime: function() { return this.videoDOM.currentTime; },
      myFPS : 25.0,
      onLoad: function() {
        // TODO: this.myFPS = ...
      },
      init: function() {
        var that = this;
        this.videoDOM.addEventListener('pause', 
                   function(){
                     var t = that.videoDOM.currentTime; 
                     // TODO: on FF use this line instead of the following two lines: this.videoDOM.currentTime -= 0.0;
                     that.videoDOM.currentTime = t-0.002; 
                     that.videoDOM.currentTime = t; 
                     //console.log("pause " + t);
                   },
                   false);
        this.videoDOM.addEventListener('timeupdate', updateTimes);
      },
      addTime : function(frames, time) {
        this.jumpTime(frames, time, true);
      },
      jumpTime : function(frames, time, add) {
        if (time == undefined) {
          if (this.getFrameTime === undefined)
          {
            return;
          }
          time = frames * this.getFrameTime(); 
        }
        var ctime = add ? this.getCurrentTime() : 0;

        ctime = Math.max(0, Math.min(this.getDuration(), ctime + time)); 

        if (typeof ctime != "number" || !isFinite(ctime))
        {
          return;
        }

        // avoid the time point where frames change
        var fullFrames = ctime / this.getFrameTime();
        var startFrameBelow = Math.floor(fullFrames) * this.getFrameTime();
        //var above = Math.floor(fullFrames + 1) * this.getFrameTime();
        var framePartAvoid = 0.1;
        if (ctime - startFrameBelow < this.getFrameTime() * framePartAvoid)
        {
          ctime = startFrameBelow + this.getFrameTime() * framePartAvoid;
        }

        this.setCurrentTime(ctime);
      },
      setSource: function(src) {
        var sources = this.videoDOM.getElementsByTagName('source');
        var source = sources[0];
        source.src = src;
      },
      getSource: function() {
        var sources = this.videoDOM.getElementsByTagName('source');
        if (sources.length > 0) { return sources[0].src; }
        return "";
      },      
      getSourceName: function() {
        return this.getSource().split("/").reverse()[0]; 
      },            
      setSubs: function(subs) {
        this.mySubs = subs;
      }
    }; // ob

    ob.getFrameRate = function() {
      return this.myFPS;
    };

    ob.setFrameRate = function(fps) {
      this.myFPS = fps;
    }

    ob.getFrameTime = function() {
      return 1.0 / this.myFPS;
    };

    ob.getDuration = function () {
      return this.videoDOM.duration; 
    }

    ob.getFrame = function(t) {
      var frame = Math.max(Math.floor(parseFloat(t.toFixed(6)) * this.getFrameRate()), 0)
      return frame;
    }
    ob.getFrameCount = function() {
      return Math.round(this.getDuration() * this.getFrameRate());
    }

    ob.binFindSub = function(frame) {
      return binFindSub(this.mySubs, frame);
    }; 

    ob.init();

    
    
    return ob;
  }  

  function secToStr(sec) {
    if (isNaN(sec)) {
      return "??:??:??";
    }
    sec = Math.floor(sec);
    var hh = Math.floor(sec / 3600);
    var sh = hh * 3600;
    var mm = Math.floor((sec - sh) / 60);
    var sm = mm * 60;
    var ss = sec - sh - sm;
    var ret = (hh > 9 ? "" : "0") + hh + ":" +
              (mm > 9 ? "" : "0") + mm + ":" +
              (ss > 9 ? "" : "0") + ss;
    return ret;
  }


  function createDefaultDrawRoutines(subtitleMgr, videoMgr, ctx, draw_debug) {

    var draw_count = 0;
    var draw_last = 0;
    var draw_fps = 60.0;
    //var draw_debug = false;

    var draw_obj = {}

    function draw(timestamp) {
      // TODO: videoMgr.videoDOM usage
      // TODO: videoMgr.mySubs usage
      ++draw_count;
      if (videoMgr === null) { 
          console.log("NULL");
      } else {
          if (draw_debug) {
            var tstr = "" + videoMgr.getCurrentTime().toFixed(6);
            if (tstr.length < 13) {
              tstr = "                       ".substr(0, 13 - tstr.length) + tstr;
            } 

            if (!(tstr in draw_obj) || draw_obj[tstr] == 0) { // workaround for black frame instead of the image

              ctxBack.clearRect ( 0 , 0 , cnvBack.width, cnvBack.height); 
              ctxBack.drawImage(videoMgr.videoDOM, 0, 0, cnvBack.width, cnvBack.height);
              var imgData = ctxBack.getImageData(0, 0, cnvBack.width, cnvBack.height);
              
              let endIdx = -1;
              let longest = 0;
              let currIdx = -1;
              let len = 0;
              // RGBA
              for (let i = 0; i < imgData.data.length; i += 4) {
                if ((imgData.data[i] >= 215 - 1 && imgData.data[i] <= 215 + 1) &&
                    (imgData.data[i + 1] >= 212 - 1 && imgData.data[i + 1] <= 212 + 1)) {
                  if (false) imgData.data[i] = 0;
                  if (i == currIdx + 4) {
                    len++;
                  } else {
                    len = 1;
                  }

                  currIdx = i;
                  if (len > longest) {
                    longest = len;
                    endIdx = i;
                  }
                  
                }
              }

              ctxBack.putImageData(imgData, 0, 0);

              if (longest > 16) {
                let x = Math.floor(endIdx / 4) % cnvBack.width;
                let y = Math.floor(endIdx / 4 / cnvBack.width);
                x -= longest / 2;

                const headImage = document.getElementById('head');
                ctxBack.drawImage(headImage, x - 55, y - 70);
              }
              /*
              if (draw_arr.length === 0) {
                for (var i = 0; i < imgData.data.length; ++i) {
                  draw_arr.push(imgData.data[i]);
                }
              } else {
                for (var i = 0; i < draw_arr.length; ++i) {
                  if (i % 4 == 3) {
                    imgData.data[i] = 255;               
                    continue;
                  }
                  var tmp = imgData.data[i];
                  imgData.data[i] = (draw_arr[i] === tmp) ? 0 : 255; 
                  draw_arr[i] = tmp;
                }
                ctxBack.putImageData(imgData, 0, 0);
              }
              */

              var hash = 0;
      
              for (var i = 0; i < imgData.data.length; ++i) {
                var v = imgData.data[i];
                //hash += v;
                hash  = ((hash << 5) - hash) + v;
                hash |= 0; // int32 trick
              }
              draw_obj[tstr] = hash;
            } else {
              hash = draw_obj[tstr];
            }

            if (false && draw_count % 100 == 0) {
              infoTxtDiv.innerHTML = "";
              var arr = [];
              for (att in draw_obj) {
                arr.push([att, draw_obj[att]]);
              }
              //arr.sort(function(a, b){ return b - a});
              arr.sort();
              for (var i = 0; i < arr.length; ++i) {
                var frame = videoMgr.getFrame(parseFloat(arr[i][0].trim()));
                infoTxtDiv.innerHTML += arr[i][0] + "," + arr[i][1] + "," + frame + "<br>";
              }

            }
          }
          //ctx.drawImage(cnv, 1, 1, 800, 600);

          ctx.clearRect ( 0 , 0 , cnv.width, cnv.height ); 
          
          if (false) { // background
            ctx.fillStyle = 'rgba(0, 0, 255, 0.1)';
            ctx.fillRect( 0 , 0 , cnv.width, cnv.height ); 
          }

          if (videoMgr.videoDOM.buffered && videoMgr.getDuration()) {
            ctx.fillStyle = "#FF7F00";
            ctx.font = "30px Verdana";

            var d = videoMgr.getDuration();
            var cw = cnv.width;
            //var s = "";
            for (var i = 0; i < videoMgr.videoDOM.buffered.length; ++i) {
              var x = videoMgr.videoDOM.buffered.start(i) / d * cw;
              var w = (videoMgr.videoDOM.buffered.end(i) - videoMgr.videoDOM.buffered.start(i)) / d * cw;
              ctx.fillRect(x, 0, w, 5);
              //if (i <= 2) {
              //  s += " " + myVideo.buffered.start(i) + "," + myVideo.buffered.end(i);
              //}
            }
            //ctx.fillText(s, 20, 140);
            ctx.fillStyle = "#7F7F7F";
            for (var i = 0; i < videoMgr.videoDOM.seekable.length; ++i) {
              var x = videoMgr.videoDOM.seekable.start(i) / d * cw;
              var w = (videoMgr.videoDOM.seekable.end(i) - videoMgr.videoDOM.seekable.start(i)) / d * cw;
              ctx.fillRect(x, 5, w, 5);
              //if (i <= 2) {
              //  s += " " + myVideo.buffered.start(i) + "," + myVideo.buffered.end(i);
              //}
            }
          }

          
          if (draw_last != 0) {
            ctx.fillStyle = "#FF0000";
            ctx.font = "bold 24px Courier New";
            var newfps = (1000/(timestamp - draw_last));
            draw_fps = 0.95 * draw_fps + 0.05 * newfps;
            ctx.fillText(draw_fps.toFixed(2).toString(), cnv.width - 100, 30);
          
            /*
            if (draw_debug) { 
              ctx.fillText(tstr + ":" + hash, 20, 120);
            }*/
          }
          
          //console.log(timestamp + " " + draw_last);

          draw_last = timestamp;
          
          var fps = videoMgr.getFrameRate();
          var fc = videoMgr.getDuration() ? Math.round(videoMgr.getDuration() * fps) : "Loading";

          var frame = videoMgr.getFrame(videoMgr.getCurrentTime()); 

          // popular editors show time
          if (!isNaN(videoMgr.getDuration())) {
            var w = 240;
            ctx.strokeStyle = "red";
            ctx.strokeRect(20, 20, w, 10);

            ctx.fillRect(20, 20, w * frame / fc, 10);
          }

          var currTimeStr = secToStr(videoMgr.getCurrentTime());
          var durStr = secToStr(videoMgr.getDuration());
          ctx.fillText(currTimeStr + "/" + durStr, 20, 50);

          ctx.fillText("" + frame + "/" + fc, 20, 70);

          var idx = videoMgr.binFindSub(frame);
          ctx.fillStyle = "#FFFF00";

          ctx.font = "16px Verdana";
          var h = 16;
          if (idx >= 0) {
            var whole = videoMgr.mySubs[idx].text;
            var lines = whole.split('|', 3);
            var n = lines.length;
            for (var i = 0; i < n; ++i) {
              var txt = lines[i];
              var w = ctx.measureText(txt).width;
              
              ctx.fillText(txt, (cnv.width - w) / 2, cnv.height - 20 - h - h * (n - i) );
            }
          }

          if (draw_count % 10 == 0) {
            subtitleMgr.flattenEditSubs();

            // recolor
            videoMgr.setSubs(subtitleMgr.processSubs(videoMgr.getFrameRate(), 1 * tab2i_mintime.value, 1 * tab2i_maxtime.value, 
                                   1 * tab2i_timebetween.value, 1 * tab2i_charspersec.value, infoTxtDiv, subtitleMgr.MODE_COLORIZE));        
          }
      }
      window.requestAnimationFrame(draw);
    } // draw

    function option(name, value)
    {
      if (name == "debug") {
        draw_debug = (value === undefined) ? draw_debug : value;
        return draw_debug;
      }
    }

    
    return { "draw": draw, "opts" : option };
  } 



